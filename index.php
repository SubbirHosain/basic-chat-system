<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>
    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .jumbotron{
            min-height: 250px
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="jumbotron">

            <div id="messages"></div>

        </div>
        <div class="form-group">
                <input type="text" name="typechat" class="form-control"  id="enterchat">
        </div>
        <button type="button" id="sendbtn" class="btn btn-primary">Send</button>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/main.js"></script>

    <script>
        $(document).ready(function() {

            setInterval(function(){ updateChat(); }, 1000);
            
            function updateChat(){
                $.ajax({

                    url:'chatprocess.php',
                    type:'post',
                    data:'chatUpdate=chat',
                    success:function(result){
                        if (result) {
                            $('#messages').html(result)
                            console.log(result);
                        }
                    }

                });
            };


            $('#sendbtn').click(function() {
                /* Act on the event */
                var chatText = $('#enterchat').val();
                //clean input after sending 
                $('#enterchat').val('');
                console.log(chatText);
                $.ajax({
                    url:'chatprocess.php',
                    type:'post',
                    data:'chat='+chatText,
                    success:function(data){
                        console.log(data);
                    }
                });
            });

        });
    </script>
</body>

</html>
